const express = require ("express");
const mongoose = require ("mongoose");

// Allows our backend application to be available to our front end application
// Allows us to control the app's Cross Origin resource
const cors = require("cors");
const port = 4000;
const app = express();
const userRoutes = require("./routes/userRoutes");
const courseRoutes = require("./routes/courseRoutes")

// Connect our mongodb database
mongoose.connect("mongodb+srv://Admin:admin123@course-booking.cevr7.mongodb.net/s32-s36?retryWrites=true&w=majority", {
	useNewURLParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection;
db.on('error', ()=> console.error.bind(console, 'Connection Error'));
db.once('open', ()=> console.error.bind(console, 'Now connected in MongoDB Atlas'));

// Allows all resource to access our backend application
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}))
app.use("/users", userRoutes);
app.use("/courses", courseRoutes);

// Will use the define port number for the application whenever an environment variable is available OR will use port 4000 if none is defined.
// This syntax will allow flexibility  when using the application locally or as a hosted application
app.listen(process.env.PORT || port, ()=>{
	console.log(`API is now online on port ${process.env.PORT || port}`)

});