const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseControllers");
const auth = require("../auth")


// Route for CREATE A COURSE
/*router.post("/", (req, res)=>{

	courseController.addCourse(req.body).then(resultFromContoller => res.send(resultFromContoller))

})*/


/*ACTIVITY S34*/
router.post("/", auth.verify,(req, res)=>{

	const userData = auth.decode(req.headers.authorization);
	console.log(userData);
	console.log("This is request body")
	console.log(req.body);

	courseController.addCourse(req.body, userData).then(resultFromController => res.send(resultFromController));

});

//Route for retrieving all the courses
router.get("/all", auth.verify, (req, res) =>{

	courseController.getAllCourses().then(resultFromController => res.send(resultFromController))
}) 

router.get("/", (req, res)=>{
	courseController.getAllActive().then(resultFromController => res.send(resultFromController))
})


// Route for retrieving a specific course
router.get("/:courseId", (req, res)=>{
	console.log(req.params.courseId)

	courseController.getCourse(req.params).then(resultFromController=> res.send (resultFromController))
})

// Route for updating a course
router.put("/:courseId", auth.verify, (req, res)=>{

	const data = {
		courseId: req.params.courseId,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		updatedCourse: req.body
	}

	courseController.updateCourse(data).then(resultFromController => res.send(resultFromController));
})

/*Activity s35*/
router.put("/:courseId/archive", auth.verify, (req, res) =>{
	
	const data = auth.decode(req.headers.authorization).isAdmin
	console.log(data);
	
	courseController.getCourseToArchive(req.params, data).then(resultFromController => res.send(resultFromController))	
})



module.exports = router;