const express= require ("express")
const router = express.Router();
const userController = require("../controllers/userControllers");
const auth = require("../auth")

// For checking if email exists
router.post("/checkEmail", (req, res)=>{
userController.checkEmailExists(req.body).then (resultFromController => res.send(resultFromController));

})


// Route for user registration
router.post("/register", (req, res)=>{
	userController.registerUser(req.body).then(resultFromController=> res.send(resultFromController));

})

// Routes for user authentication
router.post("/login", (req, res)=>{
	userController.loginUser(req.body).then (resultFromController => res.send(resultFromController));
})

// Route for retrieving user details
// The "auth.verify" will act as a middleware to ensure they can get the details
router.get("/details", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	console.log(userData);

	userController.getProfile({userId: userData.id}).then(resultFromController => res.send(resultFromController));

});


/*ACTIVITY
router.post("/details", (req, res)=>{
	userController.getUserDetails(req.body).then(resultFromController => res.send(resultFromController));
})*/

/*
router.post("/enroll", auth.verify, (req, res)=>{

	let data = {
		userId: req.body.userId,
		courseId: req.body.courseId
	}
	userController.enroll(data).then(resultFromController => res.send(resultFromController))
})
*/

/*ACTIVITY S36*/
router.post("/enroll", auth.verify, (req, res) => {

	let data = {
			isAdmin: auth.decode(req.headers.authorization).isAdmin,
			userId: auth.decode(req.headers.authorization).id,
			courseId: req.body.courseId
	}

	const userData = auth.decode(req.headers.authorization);
	console.log(userData);

	if (data.isAdmin){
		res.send("Cannot Enrolled by Admin.")
	} else {
		userController.enroll(data).then(resultFromController => res.send(resultFromController))
	}
	})



module.exports = router;

